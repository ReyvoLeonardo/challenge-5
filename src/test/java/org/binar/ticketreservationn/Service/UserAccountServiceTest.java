package org.binar.ticketreservationn.Service;

import org.binar.ticketreservationn.model.UserAccount;
import org.binar.ticketreservationn.service.UserAccountService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class UserAccountServiceTest {
        @Autowired
        UserAccountService userAccountService;

        @Test
        void newUserAccount_alreadyRegistered(){
            UserAccount userAccount = new UserAccount();
            userAccount.setUsername("Erling");
            Assertions.assertThrows(Exception.class, () -> userAccountService.addUser(userAccount));
        }
}
