package org.binar.ticketreservationn.Service;

import org.binar.ticketreservationn.model.Film;
import org.binar.ticketreservationn.service.FilmService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class FilmServiceTest {
    @Autowired
    Film filmService;

    @Test
    void newFilm_AlreadyRegistered(){
        Film film =new Film();
        film.setFilmname("JohnWick");
        film.setSedangtayang(true);
        Assertions.assertThrows(Exception.class, () -> filmService.getFilmname());

    }


}
