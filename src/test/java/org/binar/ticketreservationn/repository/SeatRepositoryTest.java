package org.binar.ticketreservationn.repository;

import org.binar.ticketreservationn.model.Seat;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class SeatRepositoryTest {
    @Autowired
    SeatRepository seatRepository;

    @Test
    void insertSeats(){
        Seat seat = new Seat();
        seat.setStudioName("X");
 //     seat.setNomorkursi("X4");
        seatRepository.save(seat);
    }
}
