package org.binar.ticketreservationn.repository;

import org.binar.ticketreservationn.model.Film;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class FilmRepositoryTest {
    @Autowired
    FilmRepository filmRepository;

        @Test
    void findFilmByFilmName(){

    List<Film> filmList = filmRepository.findFilmByFilmName("John Wick");
        filmList.forEach(film -> {
        System.out.println(film.getFilmname());
    });
    }
    @Test
    void findFilmByFilmCode(){
        List<Film> filmCode = filmRepository.findFilmByFilmCode(1);
        filmCode.forEach(film -> {
            System.out.println(film.getFilmcode());
        });


        }
    @Test
    void findFilmSedangTayang(){
        List<Film> filmSedangTayang = filmRepository.findAll();
        filmSedangTayang.forEach(film -> {
            System.out.println(film.isSedangtayang());
        });
    }

    @Test
    void insertFilm(){
        Film film = new Film();
        film.setFilmname("John Wick");
        film.setSedangtayang(true);

        filmRepository.save(film);

        Film film1 = new Film();
        film1.setFilmname("John Wick 2");
        film1.setSedangtayang(true);

        filmRepository.save(film1);

        Film film2 = new Film();
        film2.setFilmname("John Wick 3");
        film2.setSedangtayang(true);

        filmRepository.save(film2);

    }
    @Test
    void testUpdateFilm_StoreProcedure(){
            filmRepository.UpdateFilm("John WIck","Fast&Furious");
    }

}

