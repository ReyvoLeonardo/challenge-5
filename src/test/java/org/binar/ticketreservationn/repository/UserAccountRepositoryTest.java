package org.binar.ticketreservationn.repository;

import org.binar.ticketreservationn.model.UserAccount;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class UserAccountRepositoryTest {
    @Autowired
    UserAccountRepository userAccountRepository;

    @Test
    void getUserAccountByUsername(){
        List<UserAccount> UserList = userAccountRepository.findUserByUserName("kundang");
        UserList.forEach(usr -> {
            System.out.println(usr.getUsername());
        });
    }

    @Test
    void insertAccount(){
        UserAccount userAccount = new UserAccount();
        userAccount.setUsername("Erling");
        userAccount.setEmailAddress("ErlingHalaand@Mancity.com");
        userAccount.setPassword("Manchesterisblue");

        userAccountRepository.save(userAccount);

    }
}
