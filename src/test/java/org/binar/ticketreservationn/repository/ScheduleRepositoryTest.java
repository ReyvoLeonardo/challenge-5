package org.binar.ticketreservationn.repository;

import org.binar.ticketreservationn.model.Film;
import org.binar.ticketreservationn.model.Schedule;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class ScheduleRepositoryTest {
    @Autowired
    ScheduleRepository scheduleRepository;
    @Autowired
    FilmRepository filmRepository;

    @Test
    void findFilmByTanggalTayang(){
        List<Film> filmList = scheduleRepository.findFilmByTanggalTayang("John Wick");
        filmList.forEach(film -> {
            System.out.println(film.getFilmname());
        });
    }


    @Test
    void InsertSchedule(){
        Film film = new Film();
        film.setFilmname("JohnWick4");
        filmRepository.save(film);

        Schedule schedule = new Schedule();
        schedule.setTanggaltayang("31 Desember 2022");
        schedule.setJammulai("18.00");
        schedule.setJamselesai("20.30");
        schedule.setHargatiket(50000);

        scheduleRepository.save(schedule);
    }

}
