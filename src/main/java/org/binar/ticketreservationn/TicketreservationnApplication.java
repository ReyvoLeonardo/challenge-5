package org.binar.ticketreservationn;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TicketreservationnApplication {

	public static void main(String[] args) {
		SpringApplication.run(TicketreservationnApplication.class, args);
	}

}
