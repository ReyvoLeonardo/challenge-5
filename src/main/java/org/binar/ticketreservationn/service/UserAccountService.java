package org.binar.ticketreservationn.service;

import org.binar.ticketreservationn.model.UserAccount;
import org.binar.ticketreservationn.repository.UserAccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class UserAccountService implements IUserService{

    @Autowired
    UserAccountRepository userAccountRepository;

    @Override
    public void addUser(UserAccount userAccount) throws Exception{
        List<UserAccount> usr = userAccountRepository.findUserByUserName(userAccount.getUsername());
        if(usr.size() > 0) {
            throw new Exception("User sudah terdaftar sebelumnya!");
        }
        userAccountRepository.save(userAccount);

    }

    @Override
    public void deleteUser(UserAccount userAccount) throws Exception{
        List<UserAccount> usr = userAccountRepository.findUserByUserName(userAccount.getUsername());
        if(usr.size() == 0){
            throw new Exception("Username tidak ada!");
        }
        userAccountRepository.save(userAccount);
    }
}
