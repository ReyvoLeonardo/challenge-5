package org.binar.ticketreservationn.service;

import org.binar.ticketreservationn.model.UserAccount;
import org.springframework.stereotype.Service;

@Service
public interface IUserService {
    void addUser(UserAccount userAccount) throws Exception;
    //void updateUser(UserAccount userAccount);
    void deleteUser(UserAccount userAccount) throws Exception;
}
