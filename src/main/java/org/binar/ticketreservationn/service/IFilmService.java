package org.binar.ticketreservationn.service;

import org.binar.ticketreservationn.model.Film;
import org.springframework.stereotype.Service;

@Service
public interface IFilmService {
    void addFilm(Film film)throws Exception;
    void updateFilm(String filmBaru)throws Exception;
    void deleteFilm(String filmName)throws Exception;
    void showFilmSedangTayang()throws Exception;
}
