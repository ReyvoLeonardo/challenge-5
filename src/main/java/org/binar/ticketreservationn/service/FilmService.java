package org.binar.ticketreservationn.service;

import org.binar.ticketreservationn.model.Film;
import org.binar.ticketreservationn.model.UserAccount;
import org.binar.ticketreservationn.repository.FilmRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class FilmService implements IFilmService{
    @Autowired
    FilmRepository filmRepository;

    @Override
    public void addFilm(Film film) throws Exception{
        List<Film> listfilm = filmRepository.findFilmByFilmName(film.getFilmname());
        if(listfilm.size() > 0) {
            throw new Exception("Film sudah terdaftar sebelumnya!");
        }
        filmRepository.save(film);

    }


    @Override
    public void updateFilm(String filmBaru) throws Exception {
        List<Film> listfilm2 = filmRepository.findFilmByFilmName(filmBaru);
        if(listfilm2.size() == 0) {
            throw new Exception("Film tidak ada!");
        }
        filmRepository.findFilmByFilmName(filmBaru) ;

    }


    @Override
    public void deleteFilm(String filmName) throws Exception {
        List<Film> listfilm3 = filmRepository.findFilmByFilmName(filmName);
        if(listfilm3.size() == 0) {
            throw new Exception("Film tidak ada!");
        }
        filmRepository.findFilmByFilmName(filmName);

    }


    @Override
    public void showFilmSedangTayang() throws Exception {
        List<Film> listfilm4 = filmRepository.findFilmBySedangTayang();
        if(listfilm4.size() > 0) {
            throw new Exception("Film Sedang Tayang");
        }
        filmRepository.findFilmBySedangTayang();

    }
}
