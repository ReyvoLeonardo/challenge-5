package org.binar.ticketreservationn.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
public class Film {
    @Id
    @Column(name = "filmcode")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long filmcode;

    private String filmname;
    private boolean sedangtayang;


}
