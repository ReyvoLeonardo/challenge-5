package org.binar.ticketreservationn.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
public class Schedule {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer ScheduleId;



    private String tanggaltayang;
    private String jammulai;
    private String jamselesai;
    private String studioname;
    private Integer hargatiket;

    @ManyToOne
   // @JoinColumn(name = "filmcode")
    private Film filmcodde;

}
