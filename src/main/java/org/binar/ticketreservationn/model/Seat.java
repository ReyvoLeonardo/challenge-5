package org.binar.ticketreservationn.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@Getter
@Setter
public class Seat {
    @EmbeddedId
    private SeatId seatId;
    private String StudioName;

}
