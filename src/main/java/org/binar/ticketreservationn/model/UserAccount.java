package org.binar.ticketreservationn.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@Getter
@Setter
public class UserAccount {
    @Id    // Sebagai PK/ Primary Key
    @GeneratedValue(strategy = GenerationType.AUTO) // Untuk mengeset secara otomatis penomorannya
    private Integer Id;

    private String username;
    private String EmailAddress;
    private String Password;
}
