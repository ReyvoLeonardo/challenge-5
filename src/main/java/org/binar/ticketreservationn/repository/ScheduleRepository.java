package org.binar.ticketreservationn.repository;

import org.binar.ticketreservationn.model.Film;
import org.binar.ticketreservationn.model.Schedule;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ScheduleRepository extends JpaRepository<Schedule, Integer> {
    @Query(value = "Select * From schedule where tanggal_tayang = ?1", nativeQuery = true)
    public List<Film> findFilmByTanggalTayang (@Param("tanggal_tayang") String tanggaltayang);
}
