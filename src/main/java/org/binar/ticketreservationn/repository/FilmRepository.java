package org.binar.ticketreservationn.repository;

import org.binar.ticketreservationn.model.Film;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FilmRepository extends JpaRepository<Film,Integer> {
    @Query(value = "Select * From film where filmname = ?1", nativeQuery = true)
    public List<Film> findFilmByFilmName (@Param("filmname") String filmname);

    @Query(value = "Select * From film where film_code = ?1", nativeQuery = true)
    public List<Film> findFilmByFilmCode (@Param("film_code") Integer filmcode);

    @Query(value = "Select * From Film where filmSedangTayang = ?1", nativeQuery = true)
    public List<Film> findFilmBySedangTayang ();

    @Procedure("update_film")
    public void updatefilm(String updatefilm);

    @Query(value = "CALL Update(:NameFilmFrom, :NameFilmTo)", nativeQuery = true)
    void UpdateFilm(@Param("NameFilmFrom") String NameFilmFrom, @Param("NameFilmTo") String NameFilmTo);
}
