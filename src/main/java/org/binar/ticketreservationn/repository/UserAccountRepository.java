package org.binar.ticketreservationn.repository;

import org.binar.ticketreservationn.model.UserAccount;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface UserAccountRepository extends JpaRepository<UserAccount, Integer> {
    //JPQL
    //public  List<UserAccount> findUserByUsername(String username);



    @Query (value = "select * from user_account where username =?1", nativeQuery = true)
    public List<UserAccount> findUserByUserName(@Param("username") String username);

}
