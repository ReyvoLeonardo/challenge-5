package org.binar.ticketreservationn.repository;

import org.binar.ticketreservationn.model.Seat;
import org.binar.ticketreservationn.model.SeatId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SeatRepository extends JpaRepository<Seat, String> {


}
